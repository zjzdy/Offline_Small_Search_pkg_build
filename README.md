#Offline_Small_Search_pkg_build
离线小搜的离线包打包器
------
当前版本: v0.8
##注意事项
* 必须填写所有项目，发布者和主页可填NULL
* 本程序打包的离线包供离线小搜 2.2.0及其以上版本使用
* 为保证超大量文件打包的稳定,使用x64版本

##下载
WIN32: [http://git.oschina.net/zjzdy/Offline_Small_Search_pkg_build/raw/master/bin/Offline_Small_Search_pkg_build_v0.8_x64.exe](http://git.oschina.net/zjzdy/Offline_Small_Search_pkg_build/raw/master/bin/Offline_Small_Search_pkg_build_v0.8_x64.exe)

##TODO
1. 对索引的图片进行OCR，以便检索图片里面的公式及文字